// Design and implement an application that prints a table showing a subset 
// of the Unicode characters and their numeric values. Print five number/character
// pairs per line, separated by tab characters. Print the table for numeric values
// from 32 (the space character) to 126 (the ~ character), which corresponds to 
// the printable ASCII subset of the Unicode character set. Compare your output 
// to the table in Appendix C. Unlike the table in Appendix C, the values in your 
// table can increase as they go across a row.

public class Driver {

	public static void main(String[] args) {
		
		final int START = 1, END = 126;
		final int PERLINE = 5;
		
		for (int counter = START; counter <= END; counter++)
		{
			
			System.out.print((char)counter + " " + counter + "\t");
			
			if (counter % PERLINE == 0)
				System.out.println();
		}

	}

}
