// Using the Coin class defined in this chapter, design and implement a driver 
// class called CountFlips whose main method flips a coin 100 times and counts 
// how many times each side comes up. Print the results.

public class CounterFlips {

	public static void main(String[] args) {
		final int MAX = 101;
		Coin myCoin = new Coin();
		int countHeads = 0, countTails = 0;
		
		for (int count = 1; count < MAX ; count++) 
		{
			myCoin.flip();
			if (myCoin.isHeads())
				countHeads++;
			else
				countTails++;
		}

		System.out.println("There were " + countHeads + " heads");
		System.out.println("There were " + countTails + " tails");
	}

}
