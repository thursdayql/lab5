// Design and implement an application that reads a string from
// the user, then determines and prints how many of each lowercase vowel 
// (a, e, i, o, and u) appear in the entire string. Have a separate counter 
// for each vowel. Also count and print the number of nonvowel characters.

import java.util.Scanner;


public class Driver {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		String str = "";
		
		System.out.println("Enter a sequence of characters: ");
		str = scan.nextLine();
		String strLower = str.toLowerCase();
		int countA = 0, countE = 0, countOthers = 0;
		
		for (int pos = 0; pos < strLower.length(); pos++)
		{
			char myChar = strLower.charAt(pos);
			if (myChar == 'a')
				countA++;
			else
			{
				if (myChar =='e')
					countE++;
				else
					countOthers++;
			}
		}
		
		System.out.println("There are " + countA + " letters A");
		System.out.println("There are " + countE + " letters E");
		System.out.println("There are " + countOthers + " other characters");
	}

}
