// Design and implement an application that reads a string from the user and 
// prints it one character per line.

import java.util.*;

public class Driver {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		String str = "";
		
		System.out.println("Enter a sequence of characters: ");
		str = scan.nextLine();
		
		for (int pos = 0; pos < str.length(); pos++)
		{
			System.out.println(str.charAt(pos));
		}
		
		System.out.println("");
		
		// Backwards uppercase characters
		String strUpper = str.toUpperCase();
		for (int pos = str.length() - 1; pos >= 0; pos--)
		{
			System.out.println(strUpper.charAt(pos));
		}

	}

}
